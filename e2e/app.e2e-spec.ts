import { AssessmentClientAppPage } from './app.po';

describe('mindbeacon.assessment-client-app App', () => {
  let page: AssessmentClientAppPage;

  beforeEach(() => {
    page = new AssessmentClientAppPage();
  });

  it('Welcome page title test', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to BEACON! Please sign in below to get started. If you are a new user, just click JOIN below.');
  });
});
