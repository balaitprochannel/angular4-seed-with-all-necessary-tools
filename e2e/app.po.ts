import { browser, element, by } from 'protractor';

export class AssessmentClientAppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.className('lf-header-body')).getText();
  }
}
