import { Component, OnInit } from '@angular/core';
import { Router, RouterLink, RouterModule, Routes } from '@angular/router';

import { SharedService } from './app_shared/app-shared.service';
import { AppSettings } from './app.shared'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }
}