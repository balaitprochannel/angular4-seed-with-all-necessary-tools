import 'hammerjs';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { TextMaskModule } from 'angular2-text-mask';
import { RoundProgressModule } from 'angular-svg-round-progressbar';

//Application Import
import { AppComponent } from './app.component';
import { ProductComponent } from './products/product.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { AppSharedModule } from './app_shared/app-shared.module';
import { SharedService } from './app_shared/app-shared.service';
import { CanActivateViaAuthGuard } from './app_shared/router-guards/auth.guards';
import { StyleGuideComponent } from './style_guide/style.component';
//Third party Import
import { ToasterModule, ToasterService } from 'angular2-toaster';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
} from '@angular/material';

const appRoutes: Routes = [
  { path: 'styleguide', component: StyleGuideComponent },
  { path: 'pagenotfound', component: PagenotfoundComponent },
  { path: 'product', component: ProductComponent },
  { path: '', redirectTo: '/styleguide', pathMatch: 'full' },
  { path: '**', redirectTo: '/pagenotfound' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    ReactiveFormsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    AppSharedModule,
    TextMaskModule,
    ToasterModule,
    RoundProgressModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
  ],
  declarations: [
    AppComponent,
    ProductComponent,
    PagenotfoundComponent,
    StyleGuideComponent
  ],
  providers: [CanActivateViaAuthGuard],
  bootstrap: [AppComponent],
  entryComponents: [],
  exports: []
})

export class AppModule { }