import { environment } from './../environments/environment';

export class ServiceBase {
    public baseUrl: string = environment.baseUrl;

    constructor(theBaseUrl: string) {
        if (theBaseUrl != null)
            this.baseUrl = theBaseUrl;
    }
}

export class AppSettings {
    public static readonly SiteInfo = {
        webTitle: 'Demo web app',
        webUrl: 'http://google.com',
        poweredByUrl: 'http://gmail.com',
        logoUrl: '/assets/logo.png',
        webApiUrl: '',
        versionNo: "1.0",
    }
}


