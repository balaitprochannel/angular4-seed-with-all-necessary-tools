import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import {AppSettings} from './../../app.shared'
import { SharedService } from '../app-shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent implements OnDestroy, OnInit {
  public siteInfo: any = {}; 
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private router: Router, public sharedService:SharedService) {
      this.router.events
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        (urlObj: any) => {
          // this.showSecuredMenu = (urlObj.url.includes("/routename1")
          //                         && !urlObj.url.includes("/routename2") && 
          //                         !urlObj.url.includes("/routename3")) || urlObj.url.includes("/routename4");
        }
      )
   }

  ngOnInit() {
    this.siteInfo = AppSettings.SiteInfo;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
