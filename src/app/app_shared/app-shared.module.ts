import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppFooterComponent } from './app-footer/app-footer.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { FocusDirective } from './directives/input-focus';
import { SmallnumberMask } from './directives/input-small-number';
import { SharedService } from './app-shared.service';
import { RouterModule, Routes } from '@angular/router';
@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [
    AppFooterComponent,
    AppHeaderComponent,
    FocusDirective,
    SmallnumberMask,
  ],
  exports: [
    AppFooterComponent,
    AppHeaderComponent,
    FocusDirective,
    SmallnumberMask],
  providers: [
    SharedService
  ]
})

export class AppSharedModule { }