import { Injectable } from "@angular/core";
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ServiceBase } from '../app.shared';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { SharedInfo } from './model/user-info';
import { environment } from './../../environments/environment';

@Injectable()
export class SharedService extends ServiceBase {
    public apiUrl: string;
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private sharedInfo: SharedInfo;

    constructor(private http: Http) {
        super(environment.baseUrl);
        this.sharedInfo = new SharedInfo("", "", "");
        this.sharedInfo.userId = sessionStorage.getItem("userId");
        this.sharedInfo.token = sessionStorage.getItem("token");
    }

    getUserId(): string {
        return this.sharedInfo.userId;
    }

    setUserInfo(userId, email, token) {
        if (userId != undefined && userId != null) {
            sessionStorage.setItem("userId", userId);
            this.sharedInfo.userId = userId;
        }

        if (token != undefined && token != null) {
            sessionStorage.setItem("token", token);
            this.sharedInfo.token = token;
        }

        if (email != undefined && email != null && email != '') {
            this.sharedInfo.email = email;
        }

    }

    getToken(): string {
        return this.sharedInfo.token;
    }

    logOut() {
        this.logOutOnServer();
        sessionStorage.clear();
        this.sharedInfo = new SharedInfo("", "", "");

    }

    private logOutOnServer() {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append("Authorization", "Bearer " + this.sharedInfo.token);

        let options = new RequestOptions({ headers: headers });
        let url = `${this.baseUrl}/api/account/logout`;
        this.http.post(url, null, options)
            .subscribe(x => {

            });
    }

    public static getArrayIndex(array: Array<any>, name, value): number {
        for (var i = 0; i < array.length; i++) {
            if (array[i][name] == value) {
                return i;
            }
        }

        return -1;
    }
}