import { Directive, ElementRef, Renderer } from '@angular/core';

@Directive({ selector: '[cbtFocus]' })

export class FocusDirective {
    private _el = null;
    constructor(el: ElementRef, renderer: Renderer) {
        this._el = el;
    }

    ngAfterViewInit() {     
        this._el.nativeElement.focus();
    }
}