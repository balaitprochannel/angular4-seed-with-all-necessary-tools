import {ElementRef, HostListener, Directive} from "@angular/core";
import { NgControl } from "@angular/forms";

@Directive({
    selector: '[ngModel][smallnumber]'
})

export class SmallnumberMask{
    constructor(private el: ElementRef, public model: NgControl){}

    @HostListener('input', ['$event']) onEvent($event){

        console.log('key press' + $event.keypress)
        var valArray = this.el.nativeElement.value.split("");

        for(var i=0; i<valArray.length; ++i)
        {
            valArray[i] = valArray[i].replace(/\D/g, '');
        }
        
        this.model.valueAccessor.writeValue(valArray.join(""));
        //this.model.control.setValue(valArray.join(""));
    }
}