export class SharedInfo {
    constructor(
        public userId: string,
        public email: string,
        public token:string,
    ){}
}