import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SharedService } from '../app-shared.service';

@Injectable()
export class CanActivateViaAuthGuard implements CanActivate {

  constructor(private sharedService: SharedService,  private router: Router) {}

  canActivate() {
    let authenticatedToken: string = this.sharedService.getToken();
    if (authenticatedToken!=undefined && authenticatedToken!=null && authenticatedToken.trim().length>1){
        return true
    }
    this.router.navigate(['/account']);
    return false;
  }
}