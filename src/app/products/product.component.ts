import { Component, OnInit } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ToasterModule, ToasterService, ToasterContainerComponent } from 'angular2-toaster';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  private toasterService: ToasterService;
  constructor(toasterService: ToasterService) {
    this.toasterService = toasterService;
  }

  ngOnInit() {
  }
  btnClick() {
    //alert('Hi click');
    this.toasterService.pop('success', 'Success', 'Saved the Information successfully');
  }
}
