import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-style',
  templateUrl: './style.component.html',
  styleUrls: [ './style.component.scss' ]
})

export class StyleGuideComponent {
  constructor( private route: ActivatedRoute, private router: Router ) {}

  // onAnchorClick ( ) {
  //   this.route.fragment.subscribe ( f => {
  //     const element = document.querySelector ( "#" + f )
  //     if ( element ) element.scrollIntoView ( element )
  //   });
  // }
}