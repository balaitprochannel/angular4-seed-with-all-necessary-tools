'use strict';
var gulp = require('gulp'),
 sass = require('gulp-sass'),
 wait = require('gulp-wait');
 
gulp.task('sass', function () {  
  return gulp.src('./assets/stylesheets/main.scss')
    .pipe(wait(500))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./assets/stylesheets/'));
});
 
gulp.task('sass:watch', function (e) { 
  console.log('Sass job is watching...'); 
  gulp.watch('./assets/stylesheets/**/*.scss', ['sass']);
});